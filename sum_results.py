import os
import sys
import csv
import json


RES_DIR = 'results/'
OUTPUT_CSV = 'results.csv'

def date_in_between(jsonFile, startDate, endDate):
    date = jsonFile.split('.')[0]
    dateNum = int(date.replace('-',''))
    return dateNum >= int(startDate.replace('-','')) and dateNum <= int(endDate.replace('-',''))

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('python3 sum_results.py <start-date> <end-date>')
        print('e.g. python3 sum_results.py 2018-04-15 2018-04-20')
    startDate = sys.argv[1]
    endDate = sys.argv[2]
    if int(startDate.replace('-','')) > int(endDate.replace('-', '')):
        print('start date must be no greater than end date!')


    fnames = [f for f in os.listdir(RES_DIR) if os.path.isfile(os.path.join(RES_DIR, f)) and f.endswith('.json')]
    fnames = list(filter(lambda x: date_in_between(x, startDate, endDate), fnames))

    data = {}
    domains = set()
    dates = []

    for fname in fnames:
        file = os.path.join(RES_DIR, fname)
        date = fname.split('.')[0]
        dates.append(date)
        with open(file, 'r') as jf:
            data[date] = json.load(jf)
            domains |= data[date].keys()

    csvData = []
    header = [''] + sorted(dates, key=lambda x: int(x.replace('-', '')))
    csvData.append(header)
    for domain in sorted(domains):
        row = [domain]
        for date in header[1:]:
            row.append(str(data[date].get(domain, 0)))
        csvData.append(row)

    with open(OUTPUT_CSV, 'w+') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
