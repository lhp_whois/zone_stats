Tested on Ubuntu-14.04 LTS, python3.4.3

1. zone_stat for <date>:
python3 zone_stats.py <yyyy-mm-dd>
e.g. python3 zone_stats.py 2019-04-03 > results.log

2. zone_stats for today:
python3 zone_stats.py
e.g. python3 zone_stats.py > /dev/null

3. export results to csv
python3 sum_results.py <start-date> <end-date>
e.g. python3 sum_results.py 2019-04-05 2019-04-12
output: results.csv
