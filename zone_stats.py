import os
import sys
import json
import requests
import datetime
import tarfile
import shutil
import traceback
from lxml import html
from requests.auth import HTTPBasicAuth
import logging
LOG_FILENAME = 'error.out'
logging.basicConfig(filename=LOG_FILENAME, level=logging.ERROR)

dateUrl = 'http://bestwhois.org/zone_file/{date}/'
USERNAME = 'hiber'
PASSWORD = 'Qv4GatBRrFtS'
TEMP_DIR = 'temp/'
RESULT_DIR = 'results/'

def write2txt(record):
    with open('results.txt', 'a+') as f:
        f.write(record)
        f.write('\n')

def extractLinks(url):
    res = requests.get(url, auth=HTTPBasicAuth(USERNAME, PASSWORD))
    tree = html.fromstring(res.content)
    links = tree.xpath('//a')[1:]
    urls = []
    for link in links:
        urls.append(url+link.text)
    return urls

def zone_count(date, tld):
    print(date, tld)
    # count = 0
    baseUrl = dateUrl.format(date=date) + tld + '/'
    links = extractLinks(baseUrl)
    count = domains_count(links)
    # for link in links:
    #     count += domains_count(link)
    print(count)
    write2txt('{date}, {tld}, {count}'.format(date=date, tld=tld, count=count))
    return count

def download_gz_file(url):
    req = requests.get(url, stream=True, auth=HTTPBasicAuth(USERNAME, PASSWORD))
    file = url.split('/')[-1]
    file = '.'.join(file.split('.')[:-1])
    with open(file, 'wb+') as f:
        for chunk in req.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
                f.flush()
    return file

def download_gz_files(urls):
    file = urls[0].split('/')[-1]
    file = '.'.join(file.split('.')[:-1])
    f = open(file, 'wb+')
    for url in urls:
        req = requests.get(url, stream=True, auth=HTTPBasicAuth(USERNAME, PASSWORD))
        for chunk in req.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    f.flush()
    f.close()
    return file

def lines_count(file):
    nfile = file+'.txt'
    os.rename(file, nfile)
    return sum(1 for line in open(nfile, 'r'))
    # count = 0
    # with open(nfile, 'r') as f:
    #     for line in f.readlines():
    #         if line:
    #             count += 1
    # return count

def splitRecordByDates(txtFile):
    date_tld_map = {}
    f = open(txtFile, 'r')
    if os.path.exists('results'):
        os.mkdir()
    for line in f.readlines():
        date, tld, count =  list(map(lambda x: x.strip(), line.split(',')))
        if not date_tld_map.get(date):
            date_tld_map[date] = {}
        date_tld_map[date][tld] = count

    if not os.path.exists(RESULT_DIR):
        os.mkdir(RESULT_DIR)

    for date in date_tld_map:
        fname = os.path.join(RESULT_DIR, date+'.json')
        with open(fname, 'w+') as jf:
            json.dump(date_tld_map[date], jf)

def domains_count(urls):
    if len(urls) == 0:
        return 0
    file = download_gz_files(urls)
    if os.path.exists(TEMP_DIR):
        shutil.rmtree(TEMP_DIR)
    os.mkdir(TEMP_DIR)
    tar = tarfile.open(file)
    tar.extractall(path=TEMP_DIR)
    tar.close()
    domains_file = TEMP_DIR + 'domain_names.{tld}_sorted'.format(tld=file.split('.')[0])
    count = lines_count(domains_file)
    shutil.rmtree(TEMP_DIR)
    os.remove(file)
    return count

def getTLDs(url):
    res = requests.get(url, auth=HTTPBasicAuth(USERNAME, PASSWORD))
    tree = html.fromstring(res.content)
    tlds = tree.xpath('//a/text()')[1:]
    tlds = list(map(lambda x: x.strip('/'), tlds))
    return tlds

def zone_stats(date):
    stats = {}
    baseUrl = dateUrl.format(date=date)
    tlds = getTLDs(baseUrl)
    for tld in tlds:
        try:
            stats[tld] = zone_count(date, tld)
        except:
            logging.error('error: {date}, {tld}'.format(date=date, tld=tld))
    return stats

def twoDigits(num):
    if num < 10:
        return '0' + str(num)
    else:
        return str(num)

def date2String(date):
    return '{year}-{month}-{day}'.format(year=date.year, month=twoDigits(date.month), day=twoDigits(date.day))

def dateSeries(startDate, days):
    dateStrs = []
    if days < 0:
        for i in range(days+1, 1):
            date = startDate + datetime.timedelta(days=i)
            dateStrs.append(date2String(date))
    else:
        for i in range(days):
            date = startDate + datetime.timedelta(days=i)
            dateStrs.append(date2String(date))
    return dateStrs

def zone_stats_by_dates(dateStrs):
    data = {}
    for dateStr in dateStrs:
        data[dateStr] = zone_stats(dateStr)
    return data

if __name__ == '__main__':
    if len(sys.argv) == 1:
        dt = datetime.datetime.now()
        date = '{year}-{month}-{day}'.format(year=dt.year, month=twoDigits(dt.month), day=twoDigits(dt.day))
    else:
        date = sys.argv[1]
    res = zone_stats(date)
    jsonFile = os.path.join(RESULT_DIR, date+'.json')
    with open(jsonFile, 'w+') as jf:
        json.dump(res, jf)
    # erf = open('error.out', 'r')
    # for line in erf.readlines():
    #     date, tld = line.split(':')[-1].split(',')
    #     zone_count(date.strip(), tld.strip())

    # startDate = datetime.datetime(year=2018, month=12, day=31)
    # dateStrs = dateSeries(startDate, -15)
    # res = zone_stats_by_dates(dateStrs)
    # with open('2018-12-last15days.json', 'w+') as f1:
    #     json.dump(res, f1)

    # startDate = datetime.datetime(year=2019, month=4, day=1)
    # dateStrs = dateSeries(startDate, 15)
    # res = zone_stats_by_dates(dateStrs)
    # with open('2019-04-first15days.json', 'w+') as f2:
    #     json.dump(res, f2)


    # endDate = datetime.datetime(year=2018, month=12, day=31)
    # for i in range(14, -1, -1):
    #     date = endDate - datetime.timedelta(days=i)
    #     dateStr = date2String(date)
    #     data[dateStr] = zone_stats(dateStr)
    #
    # startDate = datetime.datetime(year=2019, month=4, day=1)
    # for i in range(15):
    #     date = startDate + datetime.timedelta(days=i)
    #     dateStr = date2String(date)
    #     data[dateStr] = zone_stats(dateStr)
    # with open('res.json', 'w') as f:
    #     json.dump(date, f)
